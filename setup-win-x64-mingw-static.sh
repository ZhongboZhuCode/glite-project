#!/bin/bash

# git bash is recommended for running this script
SCRIPT_PATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
cd $SCRIPT_PATH

ARCH_COMPILER_LIB_ENV="x64-mingw-static"

export VCPKG_DEFAULT_TRIPLET=$ARCH_COMPILER_LIB_ENV
export VCPKG_DEFAULT_HOST_TRIPLET=$ARCH_COMPILER_LIB_ENV

# Install vspkg first before running this script
[ ! -d "vcpkg" ] && git clone https://github.com/microsoft/vcpkg.git &&  ./vcpkg/bootstrap-vcpkg.bat
cd vcpkg
./vcpkg install glm
./vcpkg install benchmark
./vcpkg install assimp

# build 
cd $SCRIPT_PATH
[ -d "build" ] && rm -rf build
mkdir build
cd build
cmake -G "MinGW Makefiles" .. -DVCPKG_DEFAULT_TRIPLET=$ARCH_COMPILER_LIB_ENV \
                              -DCMAKE_PREFIX_PATH=$SCRIPT_PATH/vcpkg/installed/$ARCH_COMPILER_LIB_ENV \
                              -DCMAKE_MODULE_PATH=$SCRIPT_PATH/vcpkg/installed/$ARCH_COMPILER_LIB_ENV/share/stb
make -j
