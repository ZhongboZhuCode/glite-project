#!/bin/bash

if [ -z "$OpenCV_DIR" ]; then
  echo "OpenCV_DIR environment variable is not set"
  # Do something if the variable is not set, such as displaying an error message or taking alternative actions
else
  echo "OpenCV_DIR environment variable is set to: $OpenCV_DIR"
  # Perform actions that rely on the OpenCV_DIR variable being set
fi